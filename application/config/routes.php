<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/userguide3/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'app';

$route['bo_role'] = 'backoffice/role_view';
$route['listrole'] = 'backoffice/role_list';
$route['addrole'] = 'backoffice/role_add';
$route['saverole'] = 'backoffice/role_save';
$route['deleterole'] ='backoffice/role_delete';
$route['editrole/(:any)'] = 'backoffice/role_detail/$1';

$route['bo_shift'] = 'backoffice/shift_view';
$route['listshift'] = 'backoffice/shift_list';
$route['addshift'] = 'backoffice/shift_add';
$route['saveshift'] = 'backoffice/shift_save';
$route['deleteshift'] ='backoffice/shift_delete';
$route['editshift/(:any)'] = 'backoffice/shift_detail/$1';

$route['bo_admin'] = 'backoffice/admin_view';
$route['listadmin'] = 'backoffice/admin_list';
$route['addadmin'] = 'backoffice/admin_add';
$route['saveadmin'] = 'backoffice/admin_save';
$route['deleteadmin'] ='backoffice/admin_delete';
$route['editadmin/(:any)'] = 'backoffice/admin_detail/$1';

$route['bo_gukar'] = 'backoffice/staff_view';
$route['addstaff'] = 'backoffice/staff_add';
$route['liststaff'] = 'backoffice/staff_list';
$route['addstaff'] = 'backoffice/staff_add';
$route['savestaff'] = 'backoffice/staff_save';
$route['deletestaff'] ='backoffice/staff_delete';
$route['editstaff/(:any)'] = 'backoffice/staff_edit/$1';
$route['detailstaff/(:any)'] = 'backoffice/staff_detail/$1';
$route['404_override'] = '';

$route['bo_report_all'] = 'backoffice/report_all';
$route['listreport_all'] = 'backoffice/report_all_list';
// $route['user'] = 'user/index';
$route['translate_uri_dashes'] = FALSE;
