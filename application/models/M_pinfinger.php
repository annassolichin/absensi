<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_pinfinger extends CI_Model{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    var $tab1 = 'userfinger';
    var $tab2 = 'staff';
    var $param1 = 'staff.pinfinger';
    var $param2 = 'userfinger.pin';
    var $col_order = array(null,'pin','name','privilege','finger','face','password','rfid, vein, template, created_at, uuid');
    var $col_search = array('pin','name','privilege','finger','face','password','rfid, vein, template, created_at, uuid');

    public function listing($where="")
    {

        $this->db->select("id, pin, name, privilege, finger, face, password, rfid, vein, template, created_at, uuid");
        if($where!=""){
            $this->db->where($where);
        }
        $data = $this->db->get($this->tab1);
        return $data->result();
    }

    public function listing_join($where="")
    {

        $this->db->select("userfinger.id, userfinger.pin, userfinger.name as namefinger, userfinger.privilege, userfinger.finger, userfinger.face, 
                        userfinger.password, userfinger.rfid, userfinger.vein, userfinger.template, userfinger.created_at, 
                        userfinger.uuid, staff.whatsapp, staff.nip, staff.name, staff.role, staff.jurusan, staff.pinfinger, staff.passfinger, staff.register_date ");
        $this->db->from($this->tab1);
        $this->db->join($this->tab2, "$this->param1=$this->param2",'left');
        if($where!=""){
            $this->db->where($where);
        }
        $data = $this->db->get();
        return $data->result();
    }
    


    
    public function getPin($pin)
    {
        $res="";
        $this->db->select("pin, name");
        $this->db->where("pin",$pin);
        $data = $this->db->get($this->tab1);
        if(count($data->result())==0)
        {
            return true;
        }else{
            return false;
        }
    }
    public function _get_datatables_query($where,$like)
    {
        $this->db->select("userfinger.id, userfinger.pin, userfinger.name, userfinger.privilege, userfinger.finger, 
        staff.whatsapp, userfinger.face, userfinger.password, userfinger.rfid, userfinger.vein, userfinger.template, 
        userfinger.created_at, userfinger.uuid");
        $this->db->from($this->tab1);
        $this->db->join($this->tab2, "$this->param1=$this->param2",'left');
        if ($where!="") {
            $this->db->where($where);
        };
        if ($like!="") {
            $this->db->like($like);
        };
        $this->db->order_by("pin", "desc");
        $i=0;
        foreach($this->col_search as $item) {
            if($_POST['search']['value'])
    		{
    			if($i===0)
    			{
    				$this->db->group_start();
    				$this->db->like($item, $_POST['search']['value']);
    			}else{
    				$this->db->or_like($item, $_POST['search']['value']);
    			}

    			if(count($this->col_search) -1 == $i)
    				$this->db->group_end();
    		}
    		$i++;
        }
        if(isset($_POST['order']))
    	{
    		$this->db->order_by($this->col_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order)){
        	$order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }

    }

    function get_datatables($where="",$like="")
    {
        $this->_get_datatables_query($where,$like);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered($where="",$like="")
    {
        $this->_get_datatables_query($where,$like);
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function count_all($where="",$like="")
    {
        
        $this->db->select("userfinger.id, userfinger.pin, userfinger.name, userfinger.privilege, userfinger.finger, 
        staff.whatsapp, userfinger.face, userfinger.password, userfinger.rfid, userfinger.vein, userfinger.template, 
        userfinger.created_at, userfinger.uuid");
        $this->db->from($this->tab1);
        $this->db->join($this->tab2, "$this->param1=$this->param2",'left');
        if ($where!="") {
            $this->db->where($where);
        };
        if ($like!="") {
            $this->db->like($like);
        };
        return $this->db->count_all_results();
    } 
}
