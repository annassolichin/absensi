<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url','download','file'));
		// $this->load->helper("my_helper");
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->library('form_validation');
		$this->load->model('m_user');
		$this->load->model('m_conf');
		$this->load->model('m_staff');
		$this->load->model('m_wablas');
		$this->load->library('encrypt');
	}

    public function index()
    {
        // echo md5("1");
		echo $this->encryption->encrypt("12345678");
	}
}