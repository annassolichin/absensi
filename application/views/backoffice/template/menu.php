
		<!--**********************************
            Header start
        ***********************************-->
        <div class="header">
            <div class="header-content">
                <nav class="navbar navbar-expand">
                    <div class="collapse navbar-collapse justify-content-between">
                        <div class="header-left">
                           
                        </div>
                        <ul class="navbar-nav header-right">
							
							
                            <li class="nav-item dropdown header-profile">
                                <a class="nav-link" href="javascript:void(0)" role="button" data-bs-toggle="dropdown">
									<div class="header-info">
										<span class="text-black">Halo, <strong><?php echo $username;?></strong></span>
										<p class="fs-12 mb-0"><?php echo $role;?></p>
									</div>
                                    <img src="./public/theme/images/profile/17.jpg" width="20" alt="">
                                </a>
                                <div class="dropdown-menu dropdown-menu-end">
                                    <a href="./backoffice/profile" class="dropdown-item ai-icon">
                                        <svg id="icon-user1" xmlns="http://www.w3.org/2000/svg" class="text-primary" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
                                        <span class="ms-2">Profile </span>
                                    </a>
                                    <a href="./backoffice/logout" class="dropdown-item ai-icon">
                                        <svg id="icon-logout" xmlns="http://www.w3.org/2000/svg" class="text-danger" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path><polyline points="16 17 21 12 16 7"></polyline><line x1="21" y1="12" x2="9" y2="12"></line></svg>
                                        <span class="ms-2">Logout </span>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!--**********************************
            Header end ti-comment-alt
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
          <div class="deznav">
            <div class="deznav-scroll">
				<ul class="metismenu" id="menu">
                    <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
							<i class="fa-regular fa-chart-bar"></i>
							<span class="nav-text">Dashboard</span>
						</a>
                        <ul aria-expanded="false">
                            <li><a href="#">System</a></li>
							<li><a href="#">Absensi</a></li>
						</ul>
                    </li>
					<li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
							<i class="flaticon-381-network"></i>
							<span class="nav-text">Data Master</span>
						</a>
                        <ul aria-expanded="false">
							<li><a href="<?php echo base_url();?>bo_role">Jabatan</a></li>
							<li><a href="<?php echo base_url();?>bo_gukar">Guru/Karyawan</a></li>
							<li><a href="<?php echo base_url();?>bo_admin">Admin</a></li>
						</ul>
                    </li>
                    <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
							<i class="fa fa-gear fa-bold"></i>
							<span class="nav-text">Pengaturan</span>
						</a>
                        <ul aria-expanded="false">
                            <li><a href="<?php echo base_url();?>bo_shift">Shift</a></li>
                            <!-- <li><a href="#">Jam Masuk-Pulang</a></li> -->
                        </ul>
                    </li>
                    <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
							<i class="fa fa-print"></i>
							<span class="nav-text">Laporan</span>
						</a>
                        <ul aria-expanded="false">
                            <li><a href="<?php echo base_url();?>/bo_report_all">Semua</a></li>
                            <li><a href="#">Per Jurusan</a></li>
                            <li><a href="#">Per Guru</a></li>
                            <li><a href="#">Non Guru</a></li>
                        </ul>
                    </li>
                    <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
							<i class="fa fa-code"></i>
							<span class="nav-text">Log</span>
						</a>
                        <ul aria-expanded="false">
                            <li><a href="#">Log Mesin Absen</a></li>
                            <li><a href="#">Log  Whatsapp</a></li>
                        </ul>
                    </li>
                </ul>
				
				<div class="copyright">
					<p><strong><?php echo $name_app;?> </strong> © 2023 All Rights Reserved</p>
					<p>Made with <span class="heart"></span> by Eka Annas</p>
				</div>
			</div>
        </div>
        <!--**********************************
            Sidebar end
        ***********************************-->