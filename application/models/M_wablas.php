<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_wablas extends CI_Model{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    var $tab1 = 'logwablas';
    var $col_order = array(null,'whatsapp', 'message','status','created_at','updated_at');
    var $col_search = array('whatsapp', 'message','status','created_at','updated_at');

    public function listing_ontime($where="")
    {
        $this->db->select("id, whatsapp, message, status, created_at, updated_at");
        $this->db->where("created_at >","2023-11-16 00:00:00");
        $this->db->where("created_at <","2023-11-16 23:59:59");
        $this->db->where("status","0");
        $data = $this->db->get($this->tab1);
        return $data->result();
    }
    
    public function listing($where="")
    {

        $this->db->select("id, whatsapp, message, status, created_at, updated_at");
        if($where!=""){
            $this->db->where($where);
        }
        $data = $this->db->get($this->tab1);
        return $data->result();
    }
}