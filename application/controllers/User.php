<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url','download','file'));
		// $this->load->helper("my_helper");
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->library('form_validation');
		$this->load->model('m_user');
		// $this->load->library('encrypt');
	}
    public function index(){
        $data_user = $this->m_user->listing();
        $data["user"] = $data_user;
        $data["usere"]= $data_user;
        echo json_encode($data);
    }
}