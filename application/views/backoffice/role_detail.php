
<?php echo $head;?>
    <!--*******************
        Preloader end
    ********************-->

    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <a href="index.html" class="brand-logo">
                <img class="logo-abbr" src="./images/logo.png" alt="">
                <img class="logo-compact" src="./images/logo-text.png" alt="">
                <img class="brand-title" src="./images/logo-text.png" alt="">
            </a>

            <div class="nav-control">
                <div class="hamburger">
                    <span class="line"></span><span class="line"></span><span class="line"></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->
		
		<?php echo $menu;?>
		
		<!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <!-- row -->
			<div class="container-fluid">
				<div class="form-head mb-4">
					<h2 class="text-black font-w600 mb-0">Data Jabatan</h2>
				</div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="col-2">
                                    <h3>Ubah Data</h3>
                                </div>
                                <div class="col-10">
                                    
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="basic-form">
                                    <form action=""  enctype="multipart/form-data" id="formInput" method="POST">
                                        <?php foreach($role as $r){?>
                                            <input type="hidden" id="id" name="id" value="<?php echo $r->id;?>">
                                        <div class="row">
                                            <div class="mb-3 col-md-6">
                                                <label class="form-label">Nama Jabatan</label>
                                                <input type="text" class="form-control" id="name" name="name" value="<?php echo $r->name;?>" required>
                                            </div>
                                        </div>
                                        <hr>
                                        <button type="submit" class="btn btn-primary text-right">Simpan</button>
                                        <a href="<?php echo base_url();?>/bo_role" class="btn btn-primary  text-right">Kembali</a>
                                        <?php } ?>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
            </div>
        </div>
        
        <!--**********************************
            Content body end
        ***********************************-->

        <!--**********************************
            Footer start
        ***********************************-->
        
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
   
		<?php echo $foot;?>
      
	
    <!-- Datatable -->
    <script src="./public/theme/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="./public/theme/js/plugins-init/datatables.init.js"></script>
    <script>
       
        // function hapus(){
        //     var id = $("#id_data_hapus").val();
        //     $.ajax({
        //         url: "../deleterole",
        //         type: "POST",
        //         data:  {id,id},
        //         dataType:'json', 
        //         success: function(data)
        //         {
        //             if(data.hasil==1){
        //                 Swal.fire({
        //                     icon: 'success',
        //                     title: data.pesan,
        //                     showConfirmButton: false,
        //                     timer: 1000
        //                 })
        //             }else{
        //                 Swal.fire({
        //                     icon: 'warning',
        //                     title: data.pesan,
        //                     showConfirmButton: false,
        //                     timer: 1000
        //                 })
        //             }
        //             refreshData();
        //             $('#modalHapusData').modal('hide');
        //             $('#id_data_hapus').val("");
        //         }         
        //     });
        // }
        $(function () {
            $('#formInput').on('submit', function (e) {
                e.preventDefault();
                $.ajax({
                    url: "<?php echo base_url();?>/saverole",
                    type: "POST",
                    data:  new FormData(this),
                    contentType: false,
                    cache: false,
                    processData:false,
                    dataType:'json', 
                    success: function(data){
                        if(data.hasil==1){
                            Swal.fire({
                                icon: 'success',
                                title: data.pesan,
                                showConfirmButton: false,
                                timer: 1000
                            }).then(function() {
                                window.location.href = "<?php echo base_url();?>/bo_role";
                            });
                        }else{
                            Swal.fire({
                                icon: 'warning',
                                title: data.pesan,
                                showConfirmButton: false,
                                timer: 1000
                            }).then(function() {
                                window.location.href = "<?php echo base_url();?>/bo_role";
                            });
                        }
                    }   
                });
            });
        });
</script>
	
</body>
</html>