<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


function generate_date($date)
{
    $date_return = substr($date,6,4)."-".substr($date,3,2)."-".substr($date,0,2);
    return $date_return;
}
function re_generate_date($date)
{
    // 2023-06-11;
    $date_return = substr($date,8,2)."-".substr($date,5,2)."-".substr($date,0,4);
    return $date_return;
}