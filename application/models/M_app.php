<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_app extends CI_Model{

	//wget https://presensi.demoannas.my.id/app/cronsendwablas
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
	// FUNCTION AUTHENTICATION
    public function getLogin($e,$p)
	{
		$query = $this->db->query("SELECT id, email, username, password, role, status FROM user WHERE email='".$e."'  ");
		foreach($query->result() as $data)
		{
			$pass = $data->password;
			$passwordenc =  $this->encryption->decrypt($pass);
			if($data->status=="0"){
				return false;
			}else if($passwordenc==$p)
			{
				$sess_data['id'] = $data->id;
				$sess_data['role'] = $data->role;
				$sess_data['logged_in'] = "yes";
				$this->session->set_userdata($sess_data);
				return true;
			}else{
				return false;
			}
		}
	}
	public function getStatus($e)
	{
		$status="";
		$this->db->select("id, username, email, role, status");
        $this->db->where("email",$e);
        $data = $this->db->get("user");
        foreach ($data->result() as $data) {
			$status = $data->status;
		}
		return $status;
	}
	public function listing_jurusan($where="")
    {

        $this->db->select("id, name");
        if($where!=""){
            $this->db->where($where);
        }
        $data = $this->db->get("jurusan");
        return $data->result();
    }
    

}