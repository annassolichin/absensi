<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url','download','file'));
		// $this->load->helper("my_helper");
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->library('form_validation');
		$this->load->model('m_user');
		$this->load->model('m_conf');
		$this->load->model('m_staff');
		$this->load->model('m_wablas');
		$this->load->model('m_pinfinger');
		$this->load->model('m_log');
		// $this->load->library('encrypt');
	}
    public function index(){
		$logged = $this->session->userdata("session");
		$role = $this->session->userdata("role");
        if($logged != "yes"){
            $data["app"]= getenv("APP_NAME");
            $this->load->view("login",$data);
        }else{
            redirect('dashboard');

        }
    }
    
    public function log(){
        $datenow = date('Y-m-d H:i:s');
        // get data from response webhook
        $body = file_get_contents('php://input');
        // insert log
        $datains["text_json"]=$body;
        $datains["created_at"]=date('Y-m-d H:i:s');
        $this->m_conf->InsertData('log',$datains);
        //convert json
        $response = json_decode($body);
        $type = $response->type;
        if($type=="attlog")
        {
            $datalogdet["type"]= $type;
            $datalogdet["pin"]= $response->data->pin;
            $datalogdet["scan"]= $response->data->scan;
            $datalogdet["verify"]= $response->data->verify;
            $datalogdet["status_scan"]= $response->data->status_scan;
            $datalogdet["created_at"]= $datenow;
            
			$this->db->trans_start();
            $this->m_conf->InsertData('logpresensi',$datalogdet);
            $whatsapp = $this->m_staff->getWhatsapp($response->data->pin);
            $messagewa = "Anda berhasil melakukan presensi pada :".date('d-m-Y H:i');
            $datalogwa["whatsapp"]=$whatsapp;
            $datalogwa["message"]=$messagewa;
            $datalogwa["status"]="0";
            $datalogwa["created_at"]=$datenow;
            $this->m_conf->InsertData('logwablas',$datalogwa);
            $this->db->trans_complete();
            
            $datawa = [
                "data" => [
                    [
                        'phone' => $whatsapp,
                        'message' => $messagewa,
                    ]
                ]
            ];

            $curl = curl_init();
            $token = getenv("API_WABLAS");
            $random = true;
        
            curl_setopt($curl, CURLOPT_HTTPHEADER,
                array(
                    "Authorization: $token",
                    "Content-Type: application/json"
                )
            );
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($datawa) );
            curl_setopt($curl, CURLOPT_URL,  "https://jogja.wablas.com/api/v2/send-message");
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

            $result = curl_exec($curl);
            curl_close($curl);



        }else if($type=="get_userinfo")
        {
            $datauser["pin"]=$response->data->pin;
            $datauser["name"]=$response->data->name;
            $datauser["privilege"]=$response->data->privilege;
            $datauser["finger"]=$response->data->finger;
            $datauser["face"]=$response->data->face;
            $datauser["password"]=$response->data->password;
            $datauser["rfid"]=$response->data->rfid;
            $datauser["vein"]=$response->data->vein;
            $datauser["template"]=$response->data->template;
            $datauser["created_at"]= $datenow;
            $cekpin = $this->m_pinfinger->getPin($response->data->pin);
            if($cekpin){
                $this->m_conf->InsertData('userfinger',$datauser);
            }
        }
    }
    public function getuserinfo($pin)
    {
        $cloud_id = getenv('CLOUD_ID');
        $api_token = getenv('API_TOKEN');
        $url = 'https://developer.fingerspot.io/api/get_userinfo';
        $data = '{"trans_id":"1", "cloud_id":"'.$cloud_id.'", "pin":"'.$pin.'"}';
        $authorization = "Authorization: Bearer ".$api_token;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        print_r ($result);
    }
    public function getalluser()
    {
        
        $cloud_id = getenv('CLOUD_ID');
        $api_token = getenv('API_TOKEN');
        $url = 'https://developer.fingerspot.io/api/get_all_pin';
        $data = '{"trans_id":"1", "cloud_id":"'.$cloud_id.'"}';
        $authorization = "Authorization: Bearer ".$api_token;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        print_r ($result);
    }

    public function crononetime()
    {
        $datawablas = $this->m_wablas->listing_ontime();
        $datapayload=[];
        foreach($datawablas as $dw){
            array_push($datapayload,array("phone"=>$dw->whatsapp,"message"=>$dw->message));
        }
        $datawa["data"]=$datapayload;

        $curl = curl_init();
        $token = getenv("API_WABLAS");
        $random = true;
       
        curl_setopt($curl, CURLOPT_HTTPHEADER,
            array(
                "Authorization: $token",
                "Content-Type: application/json"
            )
        );
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($datawa) );
        curl_setopt($curl, CURLOPT_URL,  "https://jogja.wablas.com/api/v2/send-message");
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

        $result = curl_exec($curl);
        curl_close($curl);
    }
    public function cronsendwablas()
    {

        $datawablas = $this->m_wablas->listing(array("status"=>"0"));
        $datapayload=[];
        foreach($datawablas as $dw){
            array_push($datapayload,array("phone"=>$dw->whatsapp,"message"=>$dw->message));
        }
        $datawa["data"]=$datapayload;

        $curl = curl_init();
        $token = getenv("API_WABLAS");
        $random = true;
       
        curl_setopt($curl, CURLOPT_HTTPHEADER,
            array(
                "Authorization: $token",
                "Content-Type: application/json"
            )
        );
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($datawa) );
        curl_setopt($curl, CURLOPT_URL,  "https://jogja.wablas.com/api/v2/send-message");
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

        $result = curl_exec($curl);
        curl_close($curl);
    }
    public function statuswablas(){
        
        $body = file_get_contents('php://input');
        $response = json_decode($body);
        $nowa=$response->phone;
        $status=$response->status;
        if($status=="sent"){
            $statusups="1";
        }elseif($status=="read"){
            $statusups="2";
        }
        $dataupdate["status"]=$statusups;
        $dataupdate["updated_at"]=date('Y-m-d H:i:s');
        $where["whatsapp"]=$nowa;
        $where["status"]="0";
        $this->m_conf->UpdateData('logwablas',$dataupdate,$where);

    }
    public function createadmindefault()
    {
        $datains["username"]="Administrator";
        $datains["email"]="admin@spam4.me";
        $datains["password"]=$this->encryption->encrypt("12345678");
        $datains["role"]="1";
        $datains["status"]="1";
        $this->db->trans_start();
        if($this->m_conf->InsertDataUUID('user',$datains)){
            $output['hasil']=1;
            $output['pesan']='Data berhasil di simpan';
        }else{                    
            $output['hasil']=0;
               $output['pesan']='Data gagal di simpan';
        }
        $this->db->trans_complete();
    }

    public function showalluser()
    {
        $data_all_user = $this->m_log->listing_log_alluser();
        foreach ($data_all_user as $dau) {
            $text_json = $dau->text_json;
        }
        $json_data = json_decode($text_json);
        return $json_data->data->pin_arr;
    }
    public function showdetailuser($pin)
    {
        $cloud_id = getenv('CLOUD_ID');
        $api_token = getenv('API_TOKEN');
        $url = 'https://developer.fingerspot.io/api/get_userinfo';
        $data = '{"trans_id":"1", "cloud_id":"'.$cloud_id.'", "pin":"'.$pin.'"}';
        $authorization = "Authorization: Bearer ".$api_token;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result);
        if($response->success==true)
        {
            // sleep(10);
            
            echo $response->success;
        }else{

        }
        // var_dump($response->success);
    }
    
}
