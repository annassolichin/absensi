<!DOCTYPE html>
<html lang="en">

<head>
   <!-- All Meta -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- <meta name="author" content="DexignZone">
	<meta name="robots" content="index, follow">
	<meta name="keywords" content="admin, admin dashboard, admin template, analytics, bootstrap, bootstrap 5, modern, responsive admin dashboard, sales dashboard, sass, ui kit, web app">
	<meta name="description" content="Looking for a sleek and modern dashboard website template for a payment management system? Our template is perfect for digital payments, transaction history, and more. Elevate your admin dashboard with stunning visuals and user-friendly functionality. Try it out today!">
	<meta property="og:title" content="MOPHY : Payment Admin Dashboard  Bootstrap 5 Template">
	<meta property="og:description" content="Looking for a sleek and modern dashboard website template for a payment management system? Our template is perfect for digital payments, transaction history, and more. Elevate your admin dashboard with stunning visuals and user-friendly functionality. Try it out today!">
	<meta property="og:image" content="https://mophy.dexignzone.com/xhtml/social-image.png">
	<meta name="format-detection" content="telephone=no"> -->

	<!-- Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Title -->
	<title><?php echo $name_app;?></title>

	<!-- Favicon icon -->
	<!-- <link rel="icon" type="image/png" sizes="16x16" href="./public/theme/images/favicon.png"> -->
    <link href="<?php echo base_url();?>/public/theme/vendor/jqvmap/css/jqvmap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="./public/theme/vendor/chartist/css/chartist.min.css">
    <link href="<?php echo base_url();?>/public/theme/vendor/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet">
    
    <!-- Clockpicker -->
    <link href="<?php echo base_url();?>/public/theme/vendor/clockpicker/css/bootstrap-clockpicker.min.css" rel="stylesheet">
    
    <!-- Datatable -->
    <link href="<?php echo base_url();?>/public/theme/vendor/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
      <link href="<?php echo base_url();?>/public/theme/css/style.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    
	

      

</head>
<body>

    <!--*******************
        Preloader start
    ********************-->
    <!-- <div id="preloader">
        <div class="sk-three-bounce">
            <div class="sk-child sk-bounce1"></div>
            <div class="sk-child sk-bounce2"></div>
            <div class="sk-child sk-bounce3"></div>
        </div>
    </div> -->