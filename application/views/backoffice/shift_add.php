
<?php echo $head;?>
    <!--*******************
        Preloader end
    ********************-->

    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <a href="index.html" class="brand-logo">
                <img class="logo-abbr" src="./images/logo.png" alt="">
                <img class="logo-compact" src="./images/logo-text.png" alt="">
                <img class="brand-title" src="./images/logo-text.png" alt="">
            </a>

            <div class="nav-control">
                <div class="hamburger">
                    <span class="line"></span><span class="line"></span><span class="line"></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->
		
		<?php echo $menu;?>
		
		<!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <!-- row -->
			<div class="container-fluid">
				<div class="form-head mb-4">
					<h2 class="text-black font-w600 mb-0">Data Shift</h2>
				</div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="col-2">
                                    <h3>Tambah Data</h3>
                                </div>
                                <div class="col-10">
                                    
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="basic-form">
                                    <form action=""  enctype="multipart/form-data" id="formInput" method="POST">
                                        <div class="row">
                                            <div class="mb-3 col-md-6">
                                                <label class="form-label">Nama Shift</label>
                                                <input type="text" class="form-control" id="name" name="name" placeholder="" required>
                                            </div>
                                            <div class="mb-3 col-md-6">
                                                <label class="form-label">Jabatan</label>
                                                <select class="form-control" id="role_id" name="role_id">
                                                    <option value="">Pilih Jabatan</option>
                                                    <?php foreach($role as $r){?>
                                                    <option value="<?php echo $r->id;?>"><?php echo $r->name;?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="mb-3 col-md-6">
                                                <label class="form-label">Jam Masuk</label>
                                                <div class="input-group clockpicker">
                                                    <input type="text" class="form-control" id="clock_in" name="clock_in" required>
                                                    <span class="input-group-text search_icon"><i class="far fa-clock"></i></span>
                                                </div>
                                            </div>
                                            <div class="mb-3 col-md-6">
                                                <label class="form-label">Jam Pulang</label>
                                                <div class="input-group clockpicker">
                                                    <input type="text" class="form-control" id="clock_out" name="clock_out" required>
                                                    <span class="input-group-text search_icon"><i class="far fa-clock"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <button type="submit" class="btn btn-primary text-right">Simpan</button>
                                        <a href="./bo_shift" class="btn btn-primary  text-right">Kembali</a>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
            </div>
        </div>
        
        <!--**********************************
            Content body end
        ***********************************-->

        <!--**********************************
            Footer start
        ***********************************-->
        
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
   
		<?php echo $foot;?>
      
	
    <!-- Datatable -->
    <script src="./public/theme/vendor/moment/moment.min.js"></script>
    <script src="./public/theme/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="./public/theme/js/plugins-init/datatables.init.js"></script>
    <script src="./public/theme/vendor/clockpicker/js/bootstrap-clockpicker.min.js"></script>
    <script>
        $(function () {
            $('#formInput').on('submit', function (e) {
                e.preventDefault();
                $.ajax({
                    url: "./saveshift",
                    type: "POST",
                    data:  new FormData(this),
                    contentType: false,
                    cache: false,
                    processData:false,
                    dataType:'json', 
                    success: function(data){
                        if(data.hasil==1){
                            Swal.fire({
                                icon: 'success',
                                title: data.pesan,
                                showConfirmButton: false,
                                timer: 1000
                            }).then(function() {
                                window.location.href = "./bo_shift";
                            });
                        }else{
                            Swal.fire({
                                icon: 'warning',
                                title: data.pesan,
                                showConfirmButton: false,
                                timer: 1000
                            }).then(function() {
                                window.location.href = "./bo_shift";
                            });
                        }
                    }   
                });
            });

            $('.clockpicker').clockpicker({
                donetext: 'Done',
            }).find('input').change(function () {
                console.log(this.value);
            });
        });
</script>
	
</body>
</html>