
<?php echo $head;?>
    <!--*******************
        Preloader end
    ********************-->

    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <a href="index.html" class="brand-logo">
                <img class="logo-abbr" src="./images/logo.png" alt="">
                <img class="logo-compact" src="./images/logo-text.png" alt="">
                <img class="brand-title" src="./images/logo-text.png" alt="">
            </a>

            <div class="nav-control">
                <div class="hamburger">
                    <span class="line"></span><span class="line"></span><span class="line"></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->
		
		<?php echo $menu;?>
		
		<!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <!-- row -->
			<div class="container-fluid">
				<div class="form-head mb-4">
					<h2 class="text-black font-w600 mb-0">Ubah Data Karyawan</h2>
				</div>
                <div class="row">
                    <div class="col-12">
                        <?php foreach($staff as $ds){?>
                        <div class="card">
                            <div class="card-header">
                                <div class="col-6">
                                    <h3>Data Karyawan <?php echo $ds->namefinger;?> - <?php echo $ds->pin;?> </h3>
                                </div>
                                <div class="col-6">
                                    
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="basic-form">
                                    <form action=""  enctype="multipart/form-data" id="formInput" method="POST">
                                    <input type="hidden" class="form-control" id="pinfinger" name="pinfinger" value="<?php echo $ds->pin;?>" placeholder="" required>
                                    <input type="hidden" class="form-control" id="password" name="password" value="<?php echo $ds->password;?>" placeholder="" required>
                                    <input type="hidden" class="form-control" id="name" name="name" value="<?php echo $ds->name;?>" placeholder="" required>

                                        <div class="row">
                                            <div class="mb-3 col-md-6">
                                                <label class="form-label">Nomor Induk Pegawai (untuk login masing-masing karyawan)</label>
                                                <input type="text" class="form-control" id="nip" name="nip" placeholder="" value="<?php echo $ds->nip;?>" required>
                                            </div>
                                            <div class="mb-3 col-md-6">
                                                <label class="form-label">Nomor Whatsapp</label>
                                                <input type="text" class="form-control" id="nowhatsapp" name="nowhatsapp" value="<?php echo $ds->whatsapp;?>" placeholder="Masukan nomor whatsapp dengan awal 62" required>
                                            </div>
                                            <div class="mb-3 col-md-6">
                                                <label class="form-label">Jurusan</label>
                                                <select class="form-control" id="jurusan" name="jurusan">
                                                    <option value="">Pilih Jurusan</option>
                                                    <?php foreach($jurusan as $j){?>
                                                        <option value="<?php echo $j->id;?>" <?php if($ds->jurusan==$j->id){ echo "selected"; }?>><?php echo $j->name;?>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="mb-3 col-md-6">
                                                <label class="form-label">Jabatan</label>
                                                <select class="form-control" id="role" name="role">
                                                    <option value="">Pilih Jabatan</option>
                                                    <?php foreach($role as $r){?>
                                                        <option value="<?php echo $r->id;?>" <?php if($ds->role==$r->id){ echo "selected"; }?>><?php echo $r->name;?>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <hr>
                                        <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Simpan</button>
                                        <a href="<?php echo base_url();?>bo_gukar" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Kembali</a>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
				</div>
            </div>
        </div>
        
        <!--**********************************
            Content body end
        ***********************************-->

        <!--**********************************
            Footer start
        ***********************************-->
        
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
   
		<?php echo $foot;?>
      
	
    <!-- Datatable -->
    <script src="./public/theme/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="./public/theme/js/plugins-init/datatables.init.js"></script>
    <script>
        $(function () {
            $('#formInput').on('submit', function (e) {
                e.preventDefault();
                $.ajax({
                    url: "<?php echo base_url();?>/savestaff",
                    type: "POST",
                    data:  new FormData(this),
                    contentType: false,
                    cache: false,
                    processData:false,
                    dataType:'json', 
                    success: function(data){
                        if(data.hasil==1){
                            Swal.fire({
                                icon: 'success',
                                title: data.pesan,
                                showConfirmButton: false,
                                timer: 1000
                            }).then(function() {
                                window.location.href = "<?php echo base_url();?>/bo_gukar";
                            });
                        }else{
                            Swal.fire({
                                icon: 'warning',
                                title: data.pesan,
                                showConfirmButton: false,
                                timer: 1000
                            }).then(function() {
                                window.location.href = "<?php echo base_url();?>/bo_gukar";
                            });
                        }
                    }   
                });
            });
        });
</script>
	
</body>
</html>