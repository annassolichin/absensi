<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_conf extends CI_Model{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function UpdateData($tableName,$data,$where){
		$res = $this->db->update($tableName,$data,$where);
		return $res;
	}
	public function DeleteData($tableName,$where){
		$res = $this->db->delete($tableName,$where);
		return $res;
	}
	public function InsertData($tableName, $data)
	{
		$res = $this->db->insert($tableName, $data);
		return $res;
	}
	public function InsertDataUUID($tableName, $data)
	{
		$this->db->set('uuid','UUID()',FALSE);
		$res = $this->db->insert($tableName,$data);
		return $res;
	}
	public function GetLastCode($tableName, $columnName){
		$date = date("ym");
		$this->db->select($columnName);
        $this->db->order_by($columnName,"DESC");
        $this->db->group_by($columnName);
        $this->db->limit(1);
		$data = $this->db->get($tableName);
		if(count($data->result())>0){	
			foreach($data->result() as $d)
			{
				$data = $d->$columnName;
			}
		}else{
			$data = "0";
		}
        return $data;
	}
}