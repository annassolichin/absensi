<!DOCTYPE html>
<html lang="en" class="h-100">

<head>
   <!-- All Meta -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="author" content="DexignZone">
	<meta name="robots" content="index, follow">
	<meta name="keywords" content="admin, admin dashboard, admin template, analytics, bootstrap, bootstrap 5, modern, responsive admin dashboard, sales dashboard, sass, ui kit, web app">
	<meta name="description" content="Looking for a sleek and modern dashboard website template for a payment management system? Our template is perfect for digital payments, transaction history, and more. Elevate your admin dashboard with stunning visuals and user-friendly functionality. Try it out today!">
	<meta property="og:title" content="MOPHY : Payment Admin Dashboard  Bootstrap 5 Template">
	<meta property="og:description" content="Looking for a sleek and modern dashboard website template for a payment management system? Our template is perfect for digital payments, transaction history, and more. Elevate your admin dashboard with stunning visuals and user-friendly functionality. Try it out today!">
	<meta property="og:image" content="https://mophy.dexignzone.com/xhtml/social-image.png">
	<meta name="format-detection" content="telephone=no">

	<!-- Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Title -->
	<title><?php echo $app;?></title>

	<!-- Favicon icon -->
	<link rel="icon" type="image/png" sizes="16x16" href="./public/images/faviconsmartkid.png">
      <link href="./public/theme/css/style.css" rel="stylesheet">
	
</head>

<body class="h-100">
    <div class="login-account">
		<div class="row h-100">
			<div class="col-lg-6 col-md-7 col-sm-12 mx-auto align-self-center">
				<div class="login-form">
                    
					<div class="login-head">
                        <img src="./public/images/smaja.png" width="100" style="margin-bottom:40px; margin-top:75px"/>
						<h3 class="title">Selamat Datang</h3>
						<p>Lihat rekap absensi anda pada sistem ini, silahkan masuk kedalam sistem dengan nip dan tanggal lahir anda</p>
					</div>
					<form action="#">
						<div class="mb-4">
							<label class="mb-1 text-black">NIP</label>
							<input type="number" id="nip" name="nip" class="form-control" placeholder="Masukkan nip anda">
						</div>
						<div class="mb-4">
							<label class="mb-1 text-black">Tanggal Lahir</label>
							<input type="text" id="tgllhr" name="tgllhr" class="form-control" placeholder="Format tanggal lahir ddmmyyyy">
                            <p>Masukkan Tanggal Lahir dengan format (ddmmyyyy) contoh:01121990</p>
						</div>
						<div class="text-center mb-4">
							<button type="submit" class="btn btn-success btn-block">Masuk</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>


    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
   <script src="./public/theme/vendor/global/global.min.js"></script>
	<script src="./public/theme/vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
   <script src="./public/theme/js/custom.min.js"></script>
    <script src="./public/theme/js/deznav-init.js"></script>

</body>

</html>