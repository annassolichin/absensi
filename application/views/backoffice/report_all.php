
<?php echo $head;?>
    <!--*******************
        Preloader end
    ********************-->

    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <a href="index.html" class="brand-logo">
                <img class="logo-abbr" src="./images/logo.png" alt="">
                <img class="logo-compact" src="./images/logo-text.png" alt="">
                <img class="brand-title" src="./images/logo-text.png" alt="">
            </a>

            <div class="nav-control">
                <div class="hamburger">
                    <span class="line"></span><span class="line"></span><span class="line"></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->
		
		<?php echo $menu;?>
		
		<!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <!-- row -->
			<div class="container-fluid">
				<div class="form-head mb-4">
					<h2 class="text-black font-w600 mb-0">Report All</h2>
				</div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="col-10">
                                    <div class="row">
                                        <div class="col-3">
                                            <input type="text" placeholder="Cari Nama" name="name_fl" id="name_fl" class="form-control" />
                                        </div>
                                        <div class="col-3">
                                            <input type="text" placeholder="Cari PIN" name="pin_fl" id="pin_fl" class="form-control" />
                                        </div>
                                        <div class="col-3">
                                            <input type="text" placeholder="Dari Tanggal" name="tgl_start_fl" id="tgl_start_fl" class="form-control" />
                                        </div>
                                        <div class="col-3">
                                            <input type="text" placeholder="Sampai Tanggal" name="tgl_end_fl" id="tgl_end_fl" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <button onclick="filterdata()" class='btn btn-outline-primary btn-sm'><i class='fa fa-filter'></i></button>
                                    <button onclick="showData()" class='btn btn-outline-primary btn-sm'><i class='fa fa-redo'></i></button>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="table" style="width: 100%;">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>PIN</th>
                                                <th>Nama</th>
                                                <!-- <th>Jumlah Masuk</th> -->
                                                <!-- <th>Jumlah Pulang</th> -->
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
            </div>
        </div>
        
        <!--**********************************
            Content body end
        ***********************************-->

        <!--**********************************
            Footer start
        ***********************************-->
        
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
   
		<?php echo $foot;?>
      
	
    <!-- Datatable -->
    <script src="./public/theme/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="./public/theme/js/plugins-init/datatables.init.js"></script>
    <script>
        var table = $("#table");

        $(function () {
            showData();
        });
        function refreshData() {
            table.DataTable().ajax.reload();
        };
        function showData(){
            $("#name_fl").val("");
            $("#pin_fl").val("");
            $("#tgl_start_fl").val("");
            if ($.fn.DataTable.isDataTable('#table') ) {
                table.DataTable().destroy();
            }
            dttable = table.DataTable({
                responsive: true,
                retrieve: true,
                searching: false,
                paging: true,
                scrollX: true,
                processing: true, //Feature control the processing indicator.
                serverSide: true, //Feature control DataTables' server-side processing mode.
                order: [],
                ajax: {
                    url: "<?php echo base_url();?>/listreport_all",
                    type: "POST",
                },
                "columnDefs": [{
                    "targets": [ 0 ], //first column / numbering column
                    "orderable": true, //set not orderable
                },],
            });
        }
        function filterdata()
        {
            var name_fl = $("#name_fl").val();
            var pin_fl = $("#pin_fl").val();
            var start_fl = $("#tgl_start_fl").val();
            if ($.fn.DataTable.isDataTable('#table') ) {
                table.DataTable().destroy();
            }
            dttable = table.DataTable({
                responsive: true,
                retrieve: true,
                scrollX: true,
                searching: false,
                processing: true, //Feature control the processing indicator.
                serverSide: true, //Feature control DataTables' server-side processing mode.
                order: [],
                ajax: {
                    type: "POST",
                    url: "<?php echo base_url();?>/listreport_all",
                    data: {
                        name : name_fl,
                        pin : pin_fl,
                        start : start_fl
                    }
                },
                "columnDefs": [{
                    "targets": [ 0 ], //first column / numbering column
                    "orderable": true, //set not orderable
                },],
            });

        }
</script>
	
</body>
</html>