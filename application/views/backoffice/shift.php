
<?php echo $head;?>
    <!--*******************
        Preloader end
    ********************-->

    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <a href="index.html" class="brand-logo">
                <img class="logo-abbr" src="./images/logo.png" alt="">
                <img class="logo-compact" src="./images/logo-text.png" alt="">
                <img class="brand-title" src="./images/logo-text.png" alt="">
            </a>

            <div class="nav-control">
                <div class="hamburger">
                    <span class="line"></span><span class="line"></span><span class="line"></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->
		
		<?php echo $menu;?>
		
		<!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <!-- row -->
			<div class="container-fluid">
				<div class="form-head mb-4">
					<h2 class="text-black font-w600 mb-0">Data Shift</h2>
				</div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="col-2">
                                    <a href="./addshift" class="btn btn-primary text-right"><span class="fa fa-plus"></span>Tambah Data</a>
                                </div>
                                <div class="col-8">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <select class="form-control" id="role_id_fl" name="role_id_fl">
                                                <option value="">Pilih Jabatan</option>
                                                <?php foreach($roles as $r){?>
                                                <option value="<?php echo $r->id;?>"><?php echo $r->name;?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text" placeholder="Cari Nama Shift" name="name_fl" id="name_fl" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <button onclick="filterdata()" class='btn btn-outline-primary btn-sm'><i class='fa fa-filter'></i></button>
                                    <button onclick="showData()" class='btn btn-outline-primary btn-sm'><i class='fa fa-redo'></i></button>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="table" style="width: 100%;">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>Jabatan</th>
                                                <th>Jam Masuk</th>
                                                <th>Jam Pulang</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
            </div>
        </div>
        
        <!--**********************************
            Content body end
        ***********************************-->

        <!--**********************************
            Footer start
        ***********************************-->
        
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
   
		<?php echo $foot;?>
      
	
    <!-- Datatable -->
    <script src="./public/theme/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="./public/theme/js/plugins-init/datatables.init.js"></script>
    <script>
        var table = $("#table");

        $(function () {
            showData();
        });
        function refreshData() {
            table.DataTable().ajax.reload();
        };
        function showData(){
            $("#name_fl").val("");
            $("#role_id_fl").val("");
            if ($.fn.DataTable.isDataTable('#table') ) {
                table.DataTable().destroy();
            }
            dttable = table.DataTable({
                responsive: true,
                retrieve: true,
                searching: false,
                paging: true,
                scrollX: true,
                processing: true, //Feature control the processing indicator.
                serverSide: true, //Feature control DataTables' server-side processing mode.
                order: [],
                ajax: {
                    url: "./listshift",
                    type: "POST",
                },
                "columnDefs": [{
                    "targets": [ 0 ], //first column / numbering column
                    "orderable": true, //set not orderable
                },],
            });
        }
        function filterdata()
        {
            var name_fl = $("#name_fl").val();
            var role_id_fl = $("#role_id_fl").val();
            if ($.fn.DataTable.isDataTable('#table') ) {
                table.DataTable().destroy();
            }
            dttable = table.DataTable({
                responsive: true,
                retrieve: true,
                scrollX: true,
                searching: false,
                processing: true, //Feature control the processing indicator.
                serverSide: true, //Feature control DataTables' server-side processing mode.
                order: [],
                ajax: {
                    type: "POST",
                    url: "./listshift",
                    data: {
                        name : name_fl,
                        role_id: role_id_fl
                    }
                },
                "columnDefs": [{
                    "targets": [ 0 ], //first column / numbering column
                    "orderable": true, //set not orderable
                },],
            });

        }
        function hapusData(id)
        {
            Swal.fire({
                title: 'Apakah yakin akan menghapus data?',
                showCancelButton: true,
                confirmButtonText: 'Hapus',
                cancelButtonText: 'Batal',
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: "<?php echo base_url();?>deleteshift",
                        type: "POST",
                        data:  {id,id},
                        dataType:'json', 
                        success: function(data)
                        {
                            if(data.hasil==1){
                                Swal.fire({
                                    icon: 'success',
                                    title: data.pesan,
                                    showConfirmButton: false,
                                    timer: 1000
                                })
                            }else{
                                Swal.fire({
                                    icon: 'warning',
                                    title: data.pesan,
                                    showConfirmButton: false,
                                    timer: 1000
                                })
                            }
                            refreshData();
                        }         
                    });
                }
            })
        }
</script>
	
</body>
</html>