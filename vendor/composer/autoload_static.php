<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInite330f40865432d4563358ce3e62ad884
{
    public static $prefixLengthsPsr4 = array (
        'D' => 
        array (
            'Dotenv\\' => 7,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Dotenv\\' => 
        array (
            0 => __DIR__ . '/..' . '/vlucas/phpdotenv/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInite330f40865432d4563358ce3e62ad884::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInite330f40865432d4563358ce3e62ad884::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInite330f40865432d4563358ce3e62ad884::$classMap;

        }, null, ClassLoader::class);
    }
}
