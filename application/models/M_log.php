<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_log extends CI_Model{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    var $tab1 = "log";
    
	public function listing_log_alluser($where="")
    {

        $this->db->select("id, text_json, created_at");
        $this->db->like("text_json","get_userid_list","both");
        
        if($where!=""){
            $this->db->where($where);
        }
        $this->db->order_by("created_at","desc");
        $this->db->limit(1);
        $data = $this->db->get($this->tab1);

        return $data->result();
    }
    
    public function get_log_detailuser($where="")
    {

    }

}