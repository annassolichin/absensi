<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backoffice extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url','download','file'));
		$this->load->helper("my_helper");
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->library('form_validation');
		$this->load->model('m_user');
		$this->load->model('m_conf');
		$this->load->model('m_staff');
		$this->load->model('m_wablas');
		$this->load->model('m_app');
		$this->load->model('m_role');
		$this->load->model('m_pinfinger');
		$this->load->model('m_report');
		$this->load->model('m_log');
		$this->load->model('m_shift');
		$this->load->library('encrypt');
	}

    public function index()
    {
        $logged = $this->session->userdata("logged_in");
        $role = $this->session->userdata("role");
        if(($logged == "yes")||($role=="1")){
            // $idakun = $this->session->userdata("id");
            // $data_akun = $this->m_user->listing_join_role(array("user.id"=>$idakun));
            // foreach($data_akun as $d){
            //     $username = $d->username;
            //     $role = $d->rolename;
            // }
            // $dtapp["username"]=$username;
            // $dtapp["role"]=$role;

            // $dtapp["name_app"]=getenv('APP_NAME');
           
            // $tmplt["header"] = $this->load->view("backoffice/template/head", $dtapp, TRUE);
			// $tmplt["menu"] = $this->load->view("backoffice/template/menu", $dtapp, TRUE);
			// $tmplt["foot"] = $this->load->view("backoffice/template/foot", $dtapp, TRUE);

            // $data["head"] = $tmplt["header"];
			// $data["menu"] = $tmplt["menu"];
			// $data["foot"] = $tmplt["foot"];

            // $this->load->view("backoffice/dashboard", $data);
            redirect('bo_gukar');
        }else{
            redirect('backoffice/login');
        }
    }
    public function loginproses(){
        $email = $this->input->post("email");
		$pass = $this->input->post("password");
		
		$login = $this->m_app->getLogin($email, $pass);

		if ($login) {
			$output['hasil'] = "1";
			$output['pesan'] = 'Selamat datang kembali';
		} else {
			$status = $this->m_app->getStatus($email);
			if($status=="0"){
				$output['hasil'] = "0";
				$output['pesan'] = 'Akun anda di nonaktifkan mohon hubungi administrator';
			}else{
				$output['hasil'] = "0";
				$output['pesan'] = 'Mohon periksa kembali email password anda';
			}
		}
		echo json_encode($output);
    }
    public function login()
    {
        $data["name_app"]=getenv('APP_NAME');
        $this->load->view("backoffice/login",$data);
    }
    public function logout()
    {
        $this->session->sess_destroy();
		redirect('backoffice');
    }

    //ROLE CONTROLLER
    public function role_view()
    {
        $logged = $this->session->userdata("logged_in");
        $role = $this->session->userdata("role");
        if(($logged == "yes")||($role=="1")){
            $idakun = $this->session->userdata("id");
            $data_akun = $this->m_user->listing_join_role(array("user.id"=>$idakun));
            foreach($data_akun as $d){
                $username = $d->username;
                $role = $d->rolename;
            }
            $dtapp["username"]=$username;
            $dtapp["role"]=$role;

            $dtapp["name_app"]=getenv('APP_NAME');
           
            $tmplt["header"] = $this->load->view("backoffice/template/head", $dtapp, TRUE);
			$tmplt["menu"] = $this->load->view("backoffice/template/menu", $dtapp, TRUE);
			$tmplt["foot"] = $this->load->view("backoffice/template/foot", $dtapp, TRUE);

            $data["head"] = $tmplt["header"];
			$data["menu"] = $tmplt["menu"];
			$data["foot"] = $tmplt["foot"];

            $this->load->view("backoffice/role", $data);
        }else{
            redirect('backoffice/login');
        }
    }
    public function role_add()
    {
        $logged = $this->session->userdata("logged_in");
        $role = $this->session->userdata("role");
        if(($logged == "yes")||($role=="1")){
            $idakun = $this->session->userdata("id");
            $data_akun = $this->m_user->listing_join_role(array("user.id"=>$idakun));
            foreach($data_akun as $d){
                $username = $d->username;
                $role = $d->rolename;
            }
            $dtapp["username"]=$username;
            $dtapp["role"]=$role;

            $dtapp["name_app"]=getenv('APP_NAME');
           
            $tmplt["header"] = $this->load->view("backoffice/template/head", $dtapp, TRUE);
			$tmplt["menu"] = $this->load->view("backoffice/template/menu", $dtapp, TRUE);
			$tmplt["foot"] = $this->load->view("backoffice/template/foot", $dtapp, TRUE);

            $data["head"] = $tmplt["header"];
			$data["menu"] = $tmplt["menu"];
			$data["foot"] = $tmplt["foot"];

            $this->load->view("backoffice/role_add", $data);
        }else{
            redirect('backoffice/login');
        }
    }
    public function role_detail($uuid)
    {
        $logged = $this->session->userdata("logged_in");
        $role = $this->session->userdata("role");
        if(($logged == "yes")||($role=="1")){
            $idakun = $this->session->userdata("id");
            $data_akun = $this->m_user->listing_join_role(array("user.id"=>$idakun));
            $data_role = $this->m_role->listing(array("uuid"=>$uuid));
            foreach($data_akun as $d){
                $username = $d->username;
                $role = $d->rolename;
            }
            $dtapp["username"]=$username;
            $dtapp["role"]=$role;

            $dtapp["name_app"]=getenv('APP_NAME');
           
            $tmplt["header"] = $this->load->view("backoffice/template/head", $dtapp, TRUE);
			$tmplt["menu"] = $this->load->view("backoffice/template/menu", $dtapp, TRUE);
			$tmplt["foot"] = $this->load->view("backoffice/template/foot", $dtapp, TRUE);

            $data["head"] = $tmplt["header"];
			$data["menu"] = $tmplt["menu"];
			$data["foot"] = $tmplt["foot"];
			$data["role"] = $data_role;

            $this->load->view("backoffice/role_detail", $data);
        }else{
            redirect('backoffice/login');
        }
    }
    public function role_save()
    {
        $logged = $this->session->userdata("logged_in");
        $role = $this->session->userdata("role");
        if(($logged == "yes")||($role=="1")){
            $id=$this->input->post("id");
            $nama = $this->input->post("name");
            $datains["name"]=$nama;
            $where["id"]=$id;

            if($id<>""){

				$this->db->trans_start();
				if($this->m_conf->UpdateData('role',$datains,$where)){
                    $output['hasil']=1;
                    $output['pesan']='Data berhasil di ubah';
                }else{                    
					$output['hasil']=0;
	           		$output['pesan']='Data gagal di ubah';
                }
				$this->db->trans_complete();
            }else{
                $this->db->trans_start();
                if($this->m_conf->InsertDataUUID('role',$datains)){
                    $output['hasil']=1;
                    $output['pesan']='Data berhasil di simpan';
                }else{                    
                    $output['hasil']=0;
                       $output['pesan']='Data gagal di simpan';
                }
                $this->db->trans_complete();

            }
		    echo json_encode($output);
        }else{
            redirect('backoffice/login');
        }
    }
    public function role_list()
    {
        $logged = $this->session->userdata("logged_in");
        $role = $this->session->userdata("role");
        if(($logged == "yes")||($role=="1")){
            $name = $this->input->post("name");
            $list = $this->m_role->get_datatables(null,array("name"=>$name));
			
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $l) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $l->name;
				$row[] = "<div class='text-center'><a href='./editrole/".$l->uuid."' class='btn btn-outline-warning btn-sm'><i class='fa fa-edit'></i> </a>
                        <button class='btn btn-outline-danger btn-sm' onclick='hapusData(".$l->id.")'><i class='fa fa-trash'></i></button></div>";				
				
				$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_role->count_all(null,array("name"=>$name)),
				"recordsFiltered" => $this->m_role->count_filtered(null,array("name"=>$name)),
				"data" => $data,
			);
			echo json_encode($output);
        }else{
            redirect('backoffice/login');
        }
    }
    public function role_delete()
    {
        $logged = $this->session->userdata("logged_in");
        $role = $this->session->userdata("role");
        if(($logged == "yes")||($role=="1")){
            $this->db->trans_start();
			$id = $this->input->post("id");
			$where['id']=$id;

			$delrol = $this->m_conf->DeleteData("role",$where);
			if($delrol){
				$output['hasil']=1;
	            $output['pesan']='Data berhasil di hapus';
			}else{
				$output['hasil']=0;
	            $output['pesan']='Data gagal di hapus';
			}
			$this->db->trans_complete();
			echo json_encode($output);
        }else{
            redirect('backoffice/login');
        }
    }

    //SHIFT CONTROLLER
    public function shift_view()
    {
        $logged = $this->session->userdata("logged_in");
        $role = $this->session->userdata("role");
        if(($logged == "yes")||($role=="1")){
            $idakun = $this->session->userdata("id");
            $data_akun = $this->m_user->listing_join_role(array("user.id"=>$idakun));
            foreach($data_akun as $d){
                $username = $d->username;
                $role = $d->rolename;
            }
            $data_role = $this->m_role->listing();

            $dtapp["username"]=$username;
            $dtapp["role"]=$role;

            $dtapp["name_app"]=getenv('APP_NAME');
           
            $tmplt["header"] = $this->load->view("backoffice/template/head", $dtapp, TRUE);
			$tmplt["menu"] = $this->load->view("backoffice/template/menu", $dtapp, TRUE);
			$tmplt["foot"] = $this->load->view("backoffice/template/foot", $dtapp, TRUE);

            $data["head"] = $tmplt["header"];
			$data["menu"] = $tmplt["menu"];
			$data["foot"] = $tmplt["foot"];
			$data["roles"] = $data_role;

            $this->load->view("backoffice/shift", $data);
        }else{
            redirect('backoffice/login');
        }
    }
    public function shift_add()
    {
        $logged = $this->session->userdata("logged_in");
        $role = $this->session->userdata("role");
        if(($logged == "yes")||($role=="1")){
            $idakun = $this->session->userdata("id");
            $data_akun = $this->m_user->listing_join_role(array("user.id"=>$idakun));
            foreach($data_akun as $d){
                $username = $d->username;
                $role = $d->rolename;
            }
            $data_role = $this->m_role->listing();
            $dtapp["username"]=$username;
            $dtapp["role"]=$role;

            $dtapp["name_app"]=getenv('APP_NAME');
           
            $tmplt["header"] = $this->load->view("backoffice/template/head", $dtapp, TRUE);
			$tmplt["menu"] = $this->load->view("backoffice/template/menu", $dtapp, TRUE);
			$tmplt["foot"] = $this->load->view("backoffice/template/foot", $dtapp, TRUE);

            $data["head"] = $tmplt["header"];
			$data["menu"] = $tmplt["menu"];
			$data["foot"] = $tmplt["foot"];
            $data["role"] = $data_role;

            $this->load->view("backoffice/shift_add", $data);
        }else{
            redirect('backoffice/login');
        }
    }
    public function shift_detail($uuid)
    {
        $logged = $this->session->userdata("logged_in");
        $role = $this->session->userdata("role");
        if(($logged == "yes")||($role=="1")){
            $idakun = $this->session->userdata("id");
            $data_akun = $this->m_user->listing_join_role(array("user.id"=>$idakun));
            $data_shift = $this->m_shift->listing_join(array("shift.uuid"=>$uuid));
            foreach($data_akun as $d){
                $username = $d->username;
                $role = $d->rolename;
            }
            $data_role = $this->m_role->listing();

            $dtapp["username"]=$username;
            $dtapp["role"]=$role;

            $dtapp["name_app"]=getenv('APP_NAME');
           
            $tmplt["header"] = $this->load->view("backoffice/template/head", $dtapp, TRUE);
			$tmplt["menu"] = $this->load->view("backoffice/template/menu", $dtapp, TRUE);
			$tmplt["foot"] = $this->load->view("backoffice/template/foot", $dtapp, TRUE);

            $data["head"] = $tmplt["header"];
			$data["menu"] = $tmplt["menu"];
			$data["foot"] = $tmplt["foot"];
			$data["shift"] = $data_shift;
			$data["role"] = $data_role;

            $this->load->view("backoffice/shift_detail", $data);
        }else{
            redirect('backoffice/login');
        }    
    }
    public function shift_save()
    {
        $logged = $this->session->userdata("logged_in");
        $role = $this->session->userdata("role");
        if(($logged == "yes")||($role=="1")){
            $id=$this->input->post("id");
            $nama = $this->input->post("name");
            $role_id = $this->input->post("role_id");
            $clock_in = $this->input->post("clock_in");
            $clock_out = $this->input->post("clock_out");
            $datains["name"]=$nama;
            $datains["role_id"]=$role_id;
            $datains["clock_in"]=$clock_in;
            $datains["clock_out"]=$clock_out;
            $where["id"]=$id;

            if($id<>""){

				$this->db->trans_start();
				if($this->m_conf->UpdateData('shift',$datains,$where)){
                    $output['hasil']=1;
                    $output['pesan']='Data berhasil di ubah';
                }else{                    
					$output['hasil']=0;
	           		$output['pesan']='Data gagal di ubah';
                }
				$this->db->trans_complete();
            }else{
                $this->db->trans_start();
                if($this->m_conf->InsertDataUUID('shift',$datains)){
                    $output['hasil']=1;
                    $output['pesan']='Data berhasil di simpan';
                }else{                    
                    $output['hasil']=0;
                       $output['pesan']='Data gagal di simpan';
                }
                $this->db->trans_complete();

            }
		    echo json_encode($output);
        }else{
            redirect('backoffice/login');
        }
    }
    public function shift_list()
    {
        $logged = $this->session->userdata("logged_in");
        $role = $this->session->userdata("role");
        if(($logged == "yes")||($role=="1")){
            $name = $this->input->post("name");
            $role_id = $this->input->post("role_id");
            $list = $this->m_shift->get_datatables(null,array("shift.name"=>$name,"shift.role_id"=>$role_id));
			
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $l) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $l->name;
				$row[] = $l->rolename;
				$row[] = substr($l->clock_in,0,5);
				$row[] = substr($l->clock_out,0,5);
				$row[] = "<div class='text-center'><a href='./editshift/".$l->uuid."' class='btn btn-outline-warning btn-sm'><i class='fa fa-edit'></i> </a>
                        <button class='btn btn-outline-danger btn-sm' onclick='hapusData(".$l->id.")'><i class='fa fa-trash'></i></button></div>";				
				
				$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_shift->count_all(null,array("shift.name"=>$name,"shift.role_id"=>$role_id)),
				"recordsFiltered" => $this->m_shift->count_filtered(null,array("shift.name"=>$name,"shift.role_id"=>$role_id)),
				"data" => $data,
			);
			echo json_encode($output);
        }else{
            redirect('backoffice/login');
        }
    }
    public function shift_delete()
    {
        $logged = $this->session->userdata("logged_in");
        $role = $this->session->userdata("role");
        if(($logged == "yes")||($role=="1")){
            $id = $this->input->post("id");
			$where['id']=$id;
            $datetimenow = date('Y-m-d h:i:s');
            $datadel["deleted_at"]=$datetimenow;
            $this->db->trans_start();
			if($this->m_conf->UpdateData('shift',$datadel,$where)){
                $output['hasil']=1;
                $output['pesan']='Data berhasil dihapus';
            }else{                    
				$output['hasil']=0;
	           	$output['pesan']='Data gagal dihapus';
            }
			$this->db->trans_complete();
		    echo json_encode($output);
        }else{
            redirect('backoffice/login');
        }
    }

    //ADMIN CONTROLLER
    public function admin_view()
    {
        $logged = $this->session->userdata("logged_in");
        $role = $this->session->userdata("role");
        if(($logged == "yes")||($role=="1")){
            $idakun = $this->session->userdata("id");
            $data_akun = $this->m_user->listing_join_role(array("user.id"=>$idakun));
            foreach($data_akun as $d){
                $username = $d->username;
                $role = $d->rolename;
            }
            $dtapp["username"]=$username;
            $dtapp["role"]=$role;

            $dtapp["name_app"]=getenv('APP_NAME');
           
            $tmplt["header"] = $this->load->view("backoffice/template/head", $dtapp, TRUE);
			$tmplt["menu"] = $this->load->view("backoffice/template/menu", $dtapp, TRUE);
			$tmplt["foot"] = $this->load->view("backoffice/template/foot", $dtapp, TRUE);

            $data["head"] = $tmplt["header"];
			$data["menu"] = $tmplt["menu"];
			$data["foot"] = $tmplt["foot"];

            $this->load->view("backoffice/admin", $data);
        }else{
            redirect('backoffice/login');
        }
        
    }
    public function admin_add()
    {
        $logged = $this->session->userdata("logged_in");
        $role = $this->session->userdata("role");
        if(($logged == "yes")||($role=="1")){
            $idakun = $this->session->userdata("id");
            $data_akun = $this->m_user->listing_join_role(array("user.id"=>$idakun));
            foreach($data_akun as $d){
                $username = $d->username;
                $role = $d->rolename;
            }
            $dtapp["username"]=$username;
            $dtapp["role"]=$role;

            $dtapp["name_app"]=getenv('APP_NAME');
           
            $tmplt["header"] = $this->load->view("backoffice/template/head", $dtapp, TRUE);
			$tmplt["menu"] = $this->load->view("backoffice/template/menu", $dtapp, TRUE);
			$tmplt["foot"] = $this->load->view("backoffice/template/foot", $dtapp, TRUE);

            $data["head"] = $tmplt["header"];
			$data["menu"] = $tmplt["menu"];
			$data["foot"] = $tmplt["foot"];

            $this->load->view("backoffice/admin_add", $data);
        }else{
            redirect('backoffice/login');
        }
    }
    public function admin_detail($uuid)
    {
        $logged = $this->session->userdata("logged_in");
        $role = $this->session->userdata("role");
        if(($logged == "yes")||($role=="1")){
            $idakun = $this->session->userdata("id");
            $data_akun = $this->m_user->listing_join_role(array("user.id"=>$idakun));
            $data_admin = $this->m_user->listing_join_role(array("user.uuid"=>$uuid));
            foreach($data_akun as $d){
                $username = $d->username;
                $role = $d->rolename;
            }
            $dtapp["username"]=$username;
            $dtapp["role"]=$role;

            $dtapp["name_app"]=getenv('APP_NAME');
           
            $tmplt["header"] = $this->load->view("backoffice/template/head", $dtapp, TRUE);
			$tmplt["menu"] = $this->load->view("backoffice/template/menu", $dtapp, TRUE);
			$tmplt["foot"] = $this->load->view("backoffice/template/foot", $dtapp, TRUE);

            $data["head"] = $tmplt["header"];
			$data["menu"] = $tmplt["menu"];
			$data["foot"] = $tmplt["foot"];
			$data["admin"] = $data_admin;

            $this->load->view("backoffice/admin_detail", $data);
        }else{
            redirect('backoffice/login');
        }
    }
    public function admin_save()
    {
        $logged = $this->session->userdata("logged_in");
        $role = $this->session->userdata("role");
        if(($logged == "yes")||($role=="1")){
            $id=$this->input->post("id");
            $username = $this->input->post("username");
            $email = $this->input->post("email");
            $password = $this->input->post("password");
            $role = "1";
            $status = $this->input->post("status");

            $datains["username"]=$username;
            $datains["email"]=$email;
            if($password!="")
            {
                $datains["password"]=$this->encryption->encrypt($password);
            }
            $datains["role"]=$role;
            $datains["status"]=$status;
            $where["id"]=$id;

            if($id<>""){

				$this->db->trans_start();
				if($this->m_conf->UpdateData('user',$datains,$where)){
                    $output['hasil']=1;
                    $output['pesan']='Data berhasil di ubah';
                }else{                    
					$output['hasil']=0;
	           		$output['pesan']='Data gagal di ubah';
                }
				$this->db->trans_complete();
            }else{
                $this->db->trans_start();
                if($this->m_conf->InsertDataUUID('user',$datains)){
                    $output['hasil']=1;
                    $output['pesan']='Data berhasil di simpan';
                }else{                    
                    $output['hasil']=0;
                       $output['pesan']='Data gagal di simpan';
                }
                $this->db->trans_complete();

            }
		    echo json_encode($output);
        }else{
            redirect('backoffice/login');
        }
    }
    public function admin_list()
    {
        $logged = $this->session->userdata("logged_in");
        $role = $this->session->userdata("role");
        if(($logged == "yes")||($role=="1")){
            $username = $this->input->post("username");
            $email = $this->input->post("email");
            $status = $this->input->post("status");
            $list = $this->m_user->get_datatables(null,array("username"=>$username,"email"=>$email,"status"=>$status));
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $l) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $l->username;
				$row[] = $l->email;
                if($l->status=="1")
                {
                    $row[] = "<label class='badge badge-success'>Aktif</label>";
                }else if($l->status=="0")
                {
                    $row[] = "<label class='badge badge-danger'>Non Aktif</label>";
                }
				$row[] = "<div class='text-center'><a href='./editadmin/".$l->uuid."' class='btn btn-outline-warning btn-sm'><i class='fa fa-edit'></i> </a>
                        <button class='btn btn-outline-danger btn-sm' onclick='hapusData(".$l->id.")'><i class='fa fa-trash'></i></button></div>";				
				$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_user->count_all(null,array("username"=>$username,"email"=>$email,"status"=>$status)),
				"recordsFiltered" => $this->m_user->count_filtered(null,array("username"=>$username,"email"=>$email,"status"=>$status)),
				"data" => $data,
			);
			echo json_encode($output);
        }else{
            redirect('backoffice/login');
        }
    }
    public function admin_delete()
    {
        $logged = $this->session->userdata("logged_in");
        $role = $this->session->userdata("role");
        if(($logged == "yes")||($role=="1")){
            $this->db->trans_start();
			$id = $this->input->post("id");
			$where['id']=$id;

			$delrol = $this->m_conf->DeleteData("user",$where);
			if($delrol){
				$output['hasil']=1;
	            $output['pesan']='Data berhasil di hapus';
			}else{
				$output['hasil']=0;
	            $output['pesan']='Data gagal di hapus';
			}
			$this->db->trans_complete();
			echo json_encode($output);
        }else{
            redirect('backoffice/login');
        }
    }

    //STAFF CONTROLLER
    public function staff_view()
    {
        $logged = $this->session->userdata("logged_in");
        $role = $this->session->userdata("role");
        if(($logged == "yes")||($role=="1")){
            $idakun = $this->session->userdata("id");
            $data_akun = $this->m_user->listing_join_role(array("user.id"=>$idakun));
            foreach($data_akun as $d){
                $username = $d->username;
                $role = $d->rolename;
            }
            $dtapp["username"]=$username;
            $dtapp["role"]=$role;

            $dtapp["name_app"]=getenv('APP_NAME');
           
            $tmplt["header"] = $this->load->view("backoffice/template/head", $dtapp, TRUE);
			$tmplt["menu"] = $this->load->view("backoffice/template/menu", $dtapp, TRUE);
			$tmplt["foot"] = $this->load->view("backoffice/template/foot", $dtapp, TRUE);

            $data["head"] = $tmplt["header"];
			$data["menu"] = $tmplt["menu"];
			$data["foot"] = $tmplt["foot"];

            $this->load->view("backoffice/staff", $data);
        }else{
            redirect('backoffice/login');
        }
        
    }
    public function staff_add()
    {
        $logged = $this->session->userdata("logged_in");
        $role = $this->session->userdata("role");
        if(($logged == "yes")||($role=="1")){
            $idakun = $this->session->userdata("id");
            $data_akun = $this->m_user->listing_join_role(array("user.id"=>$idakun));
            
            $data_role = $this->m_role->listing();
            $data_jurusan = $this->m_app->listing_jurusan(array("status"=>"1"));

            foreach($data_akun as $d){
                $username = $d->username;
                $role = $d->rolename;
            }
            $dtapp["username"]=$username;
            $dtapp["role"]=$role;

            $dtapp["name_app"]=getenv('APP_NAME');
           
            $tmplt["header"] = $this->load->view("backoffice/template/head", $dtapp, TRUE);
			$tmplt["menu"] = $this->load->view("backoffice/template/menu", $dtapp, TRUE);
			$tmplt["foot"] = $this->load->view("backoffice/template/foot", $dtapp, TRUE);

            $data["head"] = $tmplt["header"];
			$data["menu"] = $tmplt["menu"];
			$data["foot"] = $tmplt["foot"];
            $data["role"]=$data_role;
            $data["jurusan"]=$data_jurusan;

            $data_all_user = $this->m_log->listing_log_alluser();
            foreach ($data_all_user as $dau) {
                $text_json = $dau->text_json;
            }
            $json_data = json_decode($text_json);
            $data["alluser"] = $json_data->data->pin_arr;
            $this->load->view("backoffice/staff_add", $data);
        }else{
            redirect('backoffice/login');
        }
    }
    public function staff_detail($pin)
    {
        $logged = $this->session->userdata("logged_in");
        $role = $this->session->userdata("role");
        if(($logged == "yes")||($role=="1")){
            $idakun = $this->session->userdata("id");
            $data_akun = $this->m_user->listing_join_role(array("user.id"=>$idakun));
            $data_staff = $this->m_pinfinger->listing_join(array("userfinger.pin"=>$pin));
            $data_role = $this->m_role->listing();
            $data_jurusan = $this->m_app->listing_jurusan(array("status"=>"1"));
            foreach($data_akun as $d){
                $username = $d->username;
                $role = $d->rolename;
            }
            $dtapp["username"]=$username;
            $dtapp["role"]=$role;

            $dtapp["name_app"]=getenv('APP_NAME');
           
            $tmplt["header"] = $this->load->view("backoffice/template/head", $dtapp, TRUE);
			$tmplt["menu"] = $this->load->view("backoffice/template/menu", $dtapp, TRUE);
			$tmplt["foot"] = $this->load->view("backoffice/template/foot", $dtapp, TRUE);

            $data["head"] = $tmplt["header"];
			$data["menu"] = $tmplt["menu"];
			$data["foot"] = $tmplt["foot"];
			$data["staff"] = $data_staff;
            $data["role"]=$data_role;
            $data["jurusan"]=$data_jurusan;

            $this->load->view("backoffice/staff_detail", $data);
        }else{
            redirect('backoffice/login');
        }
    }
    public function staff_edit($pin)
    {
        $logged = $this->session->userdata("logged_in");
        $role = $this->session->userdata("role");
        if(($logged == "yes")||($role=="1")){
            $idakun = $this->session->userdata("id");
            $data_akun = $this->m_user->listing_join_role(array("user.id"=>$idakun));
            $data_staff = $this->m_pinfinger->listing_join(array("pin"=>$pin));
            $data_role = $this->m_role->listing();
            $data_jurusan = $this->m_app->listing_jurusan(array("status"=>"1"));
            foreach($data_akun as $d){
                $username = $d->username;
                $role = $d->rolename;
            }
            $dtapp["username"]=$username;
            $dtapp["role"]=$role;

            $dtapp["name_app"]=getenv('APP_NAME');
           
            $tmplt["header"] = $this->load->view("backoffice/template/head", $dtapp, TRUE);
			$tmplt["menu"] = $this->load->view("backoffice/template/menu", $dtapp, TRUE);
			$tmplt["foot"] = $this->load->view("backoffice/template/foot", $dtapp, TRUE);

            $data["head"] = $tmplt["header"];
			$data["menu"] = $tmplt["menu"];
			$data["foot"] = $tmplt["foot"];
			$data["staff"] = $data_staff;
            $data["role"]=$data_role;
            $data["jurusan"]=$data_jurusan;

            $this->load->view("backoffice/staff_edit", $data);
        }else{
            redirect('backoffice/login');
        }
    }
    
    public function staff_list()
    {
        $logged = $this->session->userdata("logged_in");
        $role = $this->session->userdata("role");
        if(($logged == "yes")||($role=="1")){
            $name = $this->input->post("name");
            $pin = $this->input->post("pin");
            $finger = $this->input->post("finger");
            $face = $this->input->post("face");
            $whatsapp = $this->input->post("whatsapp");
            $list = $this->m_pinfinger->get_datatables(null,array("userfinger.name"=>$name,"userfinger.pin"=>$pin,"userfinger.face"=>$face,"userfinger.finger"=>$finger,"staff.whatsapp"=>$whatsapp));
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $l) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $l->pin;
				$row[] = $l->name;
				$row[] = $l->whatsapp;
                if($l->finger=="1")
                {
                    $row[] = "<label class='badge badge-success'>Aktif</label>";
                }else if($l->finger=="0")
                {
                    $row[] = "<label class='badge badge-danger'>Non Aktif</label>";
                }
                if($l->face=="1")
                {
                    $row[] = "<label class='badge badge-success'>Aktif</label>";
                }else if($l->face=="0")
                {
                    $row[] = "<label class='badge badge-danger'>Non Aktif</label>";
                }
				$row[] = "<div class='text-center'>
                            <a href='/editstaff/".$l->pin."' class='btn btn-outline-warning btn-sm'><i class='fa fa-edit'></i></a>
                            <a href='/detailstaff/".$l->pin."' class='btn btn-outline-warning btn-sm'><i class='fa fa-eye'></i></a>
                        </div>";				
				$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_pinfinger->count_all(null,array("userfinger.name"=>$name,"userfinger.pin"=>$pin,"userfinger.face"=>$face,"userfinger.finger"=>$finger,"staff.whatsapp"=>$whatsapp)),
				"recordsFiltered" => $this->m_pinfinger->count_filtered(null,array("userfinger.name"=>$name,"userfinger.pin"=>$pin,"userfinger.face"=>$face,"userfinger.finger"=>$finger,"staff.whatsapp"=>$whatsapp)),
				"data" => $data,
			);
			echo json_encode($output);
        }else{
            redirect('backoffice/login');
        }
    }
    public function staff_delete()
    {
        $logged = $this->session->userdata("logged_in");
        $role = $this->session->userdata("role");
        if(($logged == "yes")||($role=="1")){
            $this->db->trans_start();
			$id = $this->input->post("id");
			$where['id']=$id;

			$delrol = $this->m_conf->DeleteData("userfinger",$where);
			if($delrol){
				$output['hasil']=1;
	            $output['pesan']='Data berhasil di hapus';
			}else{
				$output['hasil']=0;
	            $output['pesan']='Data gagal di hapus';
			}
			$this->db->trans_complete();
			echo json_encode($output);
        }else{
            redirect('backoffice/login');
        }
    }

    public function staff_save()
    {
        $logged = $this->session->userdata("logged_in");
        $role = $this->session->userdata("role");
        if(($logged == "yes")||($role=="1")){
            
            $name = $this->input->post("name");
            $nip = $this->input->post("nip");
            $nowhatsapp = $this->input->post("nowhatsapp");
            $jurusan = $this->input->post("jurusan");
            $pinfinger = $this->input->post("pinfinger");
            $passfinger = $this->input->post("password");
            $role = $this->input->post("role");
            $register_date = date('Y-m-d H:i:s');

            $pinfinstats = $this->m_staff->getPinFinger($pinfinger);

            $datains["nip"]=$nip;
            $datains["whatsapp"]=$nowhatsapp;
            $datains["jurusan"]=$jurusan;
            $datains["role"]=$role;
            
            if(!empty($pinfinstats))
            {
                $where["pinfinger"]=$pinfinger;
                $this->db->trans_start();
				if($this->m_conf->UpdateData('staff',$datains,$where)){
                    $output['hasil']=1;
                    $output['pesan']='Data berhasil di ubah';
                }else{                    
                    $output['hasil']=0;
                    $output['pesan']='Data gagal di ubah';
                }
				$this->db->trans_complete();
            }else{
                $datains["passfinger"]=$passfinger;
                $datains["pinfinger"]=$pinfinger;
                $datains["name"]=$name;
                $datains["register_date"]=$register_date;
                $this->db->trans_start();
                if($this->m_conf->InsertDataUUID('staff',$datains)){
                    $output['hasil']=1;
                    $output['pesan']='Data berhasil di simpan';
                }else{                    
                    $output['hasil']=0;
                       $output['pesan']='Data gagal di simpan';
                }
                $this->db->trans_complete();
            }
		    echo json_encode($output);
        }else{
            redirect('backoffice/login');
        }
    }

    public function report_all()
    {
        $logged = $this->session->userdata("logged_in");
        $role = $this->session->userdata("role");
        if(($logged == "yes")||($role=="1")){
            $idakun = $this->session->userdata("id");
            $data_akun = $this->m_user->listing_join_role(array("user.id"=>$idakun));
            foreach($data_akun as $d){
                $username = $d->username;
                $role = $d->rolename;
            }
            $dtapp["username"]=$username;
            $dtapp["role"]=$role;

            $dtapp["name_app"]=getenv('APP_NAME');
           
            $tmplt["header"] = $this->load->view("backoffice/template/head", $dtapp, TRUE);
			$tmplt["menu"] = $this->load->view("backoffice/template/menu", $dtapp, TRUE);
			$tmplt["foot"] = $this->load->view("backoffice/template/foot", $dtapp, TRUE);

            $data["head"] = $tmplt["header"];
			$data["menu"] = $tmplt["menu"];
			$data["foot"] = $tmplt["foot"];

            $this->load->view("backoffice/report_all", $data);
        }else{
            redirect('backoffice/login');
        }
        
    }

    // public function tanggal()
    // {
    //     $date = date('d/m/Y');
    //     echo "12/10/2023";
    //     echo generate_date($date);
    // }
    public function report_all_list()
    {
        $logged = $this->session->userdata("logged_in");
        $role = $this->session->userdata("role");
        if(($logged == "yes")||($role=="1")){
            $name = $this->input->post("name");
            $pin = $this->input->post("pin");
            $tgl_start = $this->input->post("start");
            // echo $tgl_start;
            if($tgl_start!=0)
            {   
                $tgl_start = generate_date($tgl_start);
                $where = $tgl_start;
                // echo $where;
            }else{
                $where= generate_date(date('d/m/Y'));
                // echo $where;
            }
            // var_dump($tgl_start);
            $list = $this->m_report->get_datatables(null,null,null,null);
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $l) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $l->pin;
				$row[] = $l->name;
				// $row[] = $l->jumlah_absen_masuk;	
				// $row[] = $l->jumlah_absen_pulang;			
				$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_report->count_all(null,null,null,null),
				"recordsFiltered" => $this->m_report->count_filtered(null,null,null,null),
				"data" => $data,
			);
			echo json_encode($output);
        }else{
            redirect('backoffice/login');
        }
        
    }
}